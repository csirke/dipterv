\select@language {magyar} \contentsline {chapter}{\numberline {1}Bevezetés}{2}{chapter.1}
\select@language {magyar} \contentsline {chapter}{\numberline {2}A technológiáról}{3}{chapter.2}
\select@language {magyar} \contentsline {section}{\numberline {2.1}Az additív technológia rövid története}{3}{section.2.1}
\select@language {magyar} \contentsline {section}{\numberline {2.2}A mai 3D nyomtatási technológiák}{4}{section.2.2}
\select@language {magyar} \contentsline {subsection}{\numberline {2.2.1}FDM eljárás}{5}{subsection.2.2.1}
\select@language {magyar} \contentsline {subsection}{\numberline {2.2.2}SLA eljárás}{7}{subsection.2.2.2}
\select@language {magyar} \contentsline {subsection}{\numberline {2.2.3}DLP eljárás}{8}{subsection.2.2.3}
\select@language {magyar} \contentsline {subsection}{\numberline {2.2.4}SLS eljárás}{9}{subsection.2.2.4}
\select@language {magyar} \contentsline {subsection}{\numberline {2.2.5}SLM eljárás}{10}{subsection.2.2.5}
\select@language {magyar} \contentsline {subsection}{\numberline {2.2.6}EBM eljárás}{10}{subsection.2.2.6}
\select@language {magyar} \contentsline {chapter}{\numberline {3}Az otthoni 3D nyomtatásról}{12}{chapter.3}
\select@language {magyar} \contentsline {section}{\numberline {3.1}A RepRap projekt}{12}{section.3.1}
\select@language {magyar} \contentsline {section}{\numberline {3.2}A 3D nyomtatás folyamata}{12}{section.3.2}
\select@language {magyar} \contentsline {section}{\numberline {3.3}Asztali FDM nyomtatók elemei}{15}{section.3.3}
\select@language {magyar} \contentsline {section}{\numberline {3.4}Asztali FDM nyomtatók típusai}{22}{section.3.4}
\select@language {magyar} \contentsline {chapter}{\numberline {4}A megvalósított nyomtató felépítése és tulajdonságai}{28}{chapter.4}
\select@language {magyar} \contentsline {section}{\numberline {4.1}A nyomtató mechanikai felépítése}{28}{section.4.1}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.1}Főbb komponensek bemutatása}{29}{subsection.4.1.1}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.2}Felépítés}{31}{subsection.4.1.2}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.3}Különleges megoldások}{35}{subsection.4.1.3}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1.4}Szükséges megmunkálások folyamata, eszközei}{39}{subsection.4.1.4}
\select@language {magyar} \contentsline {section}{\numberline {4.2}A nyomtató elektronikai felépítése}{40}{section.4.2}
\select@language {magyar} \contentsline {subsection}{\numberline {4.2.1}Alkalmazott elemek}{40}{subsection.4.2.1}
\select@language {magyar} \contentsline {subsection}{\numberline {4.2.2}Elektromos rendszer felépítése}{46}{subsection.4.2.2}
\select@language {magyar} \contentsline {subsection}{\numberline {4.2.3}Az interfészpanel megtervezése és élesztése}{47}{subsection.4.2.3}
\select@language {magyar} \contentsline {section}{\numberline {4.3}A nyomtató szoftveres háttere}{51}{section.4.3}
\select@language {magyar} \contentsline {section}{\numberline {4.4}A nyomtatóval elérhető minőség, problémák}{52}{section.4.4}
\select@language {magyar} \contentsline {chapter}{\numberline {5}A furat méretproblémájának megoldási kísérlete}{55}{chapter.5}
\select@language {magyar} \contentsline {section}{\numberline {5.1}A megoldás elve}{55}{section.5.1}
\select@language {magyar} \contentsline {section}{\numberline {5.2}Az új vezérlés hardveres bemutatása}{56}{section.5.2}
\select@language {magyar} \contentsline {section}{\numberline {5.3}Az új vezérlés szoftveres bemutatása}{57}{section.5.3}
\select@language {magyar} \contentsline {subsection}{\numberline {5.3.1}A PC oldali szoftver}{57}{subsection.5.3.1}
\select@language {magyar} \contentsline {subsection}{\numberline {5.3.2}A mikrovezérlő oldali szoftver}{61}{subsection.5.3.2}
\select@language {magyar} \contentsline {section}{\numberline {5.4}A kísérlet eredménye}{67}{section.5.4}
\select@language {magyar} \contentsline {chapter}{\numberline {6}Fejlesztési lehetőségek}{70}{chapter.6}
\select@language {magyar} \contentsline {chapter}{\numberline {7}Összefoglalás}{71}{chapter.7}
\select@language {magyar} \contentsline {chapter}{Irodalomjegyz\'ek}{72}{chapter.7}
